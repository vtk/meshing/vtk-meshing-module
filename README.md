VTK Meshing Module

# Brief

This project is an additional module for the VTK library.
It contains meshing related data sets, helpers and algorithms

** This is an early prototype, not ready for final use **

# Features

* Merge two hexa-only grids into a single hex + pyramid grids
  * There is still one missing layer of cell in between both grids, which ends up disconnected
  * Only the first meshing is available, dividing some hexa into pyramids
* This helper is aimed at helping with meshing algorithms

### Expected to come

* A linked cells mesh data model helping with cell insertion deletion
* Much more cell remeshing tools
* ParaView plugin

# How to install

The VTK Meshing module project requires the following software:

* the [CMake](https://cmake.org/) build system,
* the [VTK library](https://vtk.org/) for the module or [ParaView software](https://www.paraview.org/) for the plugin,

### VTK

VTK Meshing module needs VTK >= 9.3. It can be installed:
* using the package manager of your system (including brew on OSX, or vcpkg on Windows),
* manually using [CMake instructions](https://vtk.org/Wiki/VTK/Configure_and_Build).

### ParaView

**Not available yet**

VTK Meshing module needs ParaView >= 5.10.0. In order to build plugins, ParaView needs to
be compiled from sources manually using these
[CMake instructions](https://gitlab.kitware.com/paraview/paraview/-/blob/master/Documentation/dev/build.md).

### VTK Meshing module

VTK Meshing module can be installed using the standard CMake procedure:

1. Create a build folder
1. Launch CMake with this repository as source folder
1. Configure the project. It will need VTK to configure
   * If you have installed VTK in a custom folder, you can set the `VTK_DIR`
     variable to: **install_dir**/lib/cmake/vtk-9.3.
   * You can instead provide a custom `CMAKE_INSTALL_PREFIX` pointing to
     your VTK install folder.
   * You can use the `CMAKE_INSTALL_PREFIX` to choose where to install
     this project
1. Build and install the VTK Meshing module (please refer to the
   [VTK tutorial](https://vtk.org/Wiki/VTK/Configure_and_Build#Build_VTK)
   if you do not know how to proceed).

To use this module on your project, you will need a
`FindPackage(vtk_meshing_module)` in your project.

# How to contribute

If you want to contribute to this repository, simply open a Merge request
on the [gitlab instance](https://gitlab.kitware.com/vtk/meshing/vtk-meshing-module), we will
gladly review and help you merge your code.
