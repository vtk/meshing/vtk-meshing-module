/**
 * @class   vtkMMUnstructuredGridHelperAlgorithm
 * @brief   vtkAlgorithm with additional helpers to traverse vtkUnstructuredGrid
 *
 * TODO
 */

#ifndef vtkMMUnstructuredGridHelperAlgorithm_h
#define vtkMMUnstructuredGridHelperAlgorithm_h

// VTK includes
#include "vtkUnstructuredGridAlgorithm.h"

#include "vtkMMExecutionModelModule.h"

class vtkCell;
class vtkDataSet;
class vtkPoints;

// Filter
class VTKMMEXECUTIONMODEL_EXPORT vtkMMUnstructuredGridHelperAlgorithm
  : public vtkUnstructuredGridAlgorithm
{

public:
  static vtkMMUnstructuredGridHelperAlgorithm* New();
  vtkTypeMacro(vtkMMUnstructuredGridHelperAlgorithm, vtkUnstructuredGridAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /// vtkDataSet related helpers

  /**
   * Check for bounding box intersection between two data set
   */
  bool DataSetBBIntersect(vtkDataSet* d1, vtkDataSet* d2);

  /**
   * create an unstructured grid from a single cell
   * and the full list of points.
   */
  static bool CellToUG(vtkCell* oneCell, vtkPoints* pts, vtkUnstructuredGrid* result);

  /// Cell related helpers

  // hexa is a vtkUnstructuredGrid with a single hexa.
  // insert is the list of ids to add in the cell (from the vtkPoints of the hexa UG)
  // After the call, result is a mesh with pyramids / wedges that has the same external
  // faces but include the internal points in newly created cells.
  // For now, only 2 and 3 points insertions are supported.
  static bool HexaInsertPoints(vtkUnstructuredGrid* hexa, std::vector<vtkIdType> insert, vtkUnstructuredGrid* result);

  /// Point related helpers

  /**
   * Insert additional1 points to the result list (which may initially be non-empty)
   * return the map between old and new ids.
   * Can be use as a helper in static mode
   */
  static std::vector<vtkIdType> AppendPoints(vtkPoints* result, vtkPoints* additional1);

  /// Neighbors related helpers

protected:
  vtkMMUnstructuredGridHelperAlgorithm()           = default;
  ~vtkMMUnstructuredGridHelperAlgorithm() override = default;

private:
  vtkMMUnstructuredGridHelperAlgorithm(const vtkMMUnstructuredGridHelperAlgorithm&) = delete;
  void operator=(const vtkMMUnstructuredGridHelperAlgorithm&)                       = delete;
};

#endif
