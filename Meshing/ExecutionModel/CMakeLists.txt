set(vtk_meshing_execution_model_files
  vtkMMUnstructuredGridHelperAlgorithm
)

vtk_module_add_module(vtkMMExecutionModel
  ${FORCE_STATIC_MODULES_STRING}
  CLASSES ${vtk_meshing_execution_model_files}
)
