#include "vtkMMUnstructuredGridHelperAlgorithm.h"

// VTK related includes
#include "vtkBoundingBox.h"
#include "vtkCell.h"
#include "vtkDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPyramid.h"
#include "vtkUnstructuredGrid.h"

#include <array>
#include <numeric>
#include <vector>

vtkStandardNewMacro(vtkMMUnstructuredGridHelperAlgorithm);

//------------------------------------------------------------------------------
void vtkMMUnstructuredGridHelperAlgorithm::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

/// Data set related helpers

//------------------------------------------------------------------------------
bool vtkMMUnstructuredGridHelperAlgorithm::DataSetBBIntersect(vtkDataSet* d1, vtkDataSet* d2)
{
  double bounds[6];
  d1->GetBounds(bounds);
  vtkBoundingBox boundingBox1(bounds);
  d2->GetBounds(bounds);
  vtkBoundingBox boundingBox2(bounds);
  return boundingBox1.IntersectBox(boundingBox2);
}

//------------------------------------------------------------------------------
bool vtkMMUnstructuredGridHelperAlgorithm::CellToUG(
  vtkCell* oneCell, vtkPoints* pts, vtkUnstructuredGrid* result)
{
  if (oneCell == nullptr)
  {
    std::cerr << "oneCell should not be null" << std::endl;
    return false;
  }
  if (result == nullptr)
  {
    std::cerr << "result should not be null" << std::endl;
    return false;
  }
  if (result->GetNumberOfCells() || result->GetNumberOfPoints())
  {
    std::cerr << "result is not empty" << std::endl;
  }
  result->Initialize();
  result->AllocateEstimate(1, 10);
  result->SetPoints(pts);
  result->InsertNextCell(oneCell->GetCellType(), oneCell->GetPointIds());

  return true;
}

/// Cells related helpers

namespace
{
static bool CheckHexa(vtkUnstructuredGrid* hexa)
{
  // TODO: check for degenerate ?
  if (hexa->GetCell(0)->GetCellType() != VTK_HEXAHEDRON)
  {
    return false;
  }
  return true;
}

static bool HexaInsertOnePoint(
  vtkUnstructuredGrid* hexa, vtkIdType insert, vtkUnstructuredGrid* result)
{
  result->AllocateExact(6, 5 * 6);
  result->SetPoints(hexa->GetPoints());

  vtkNew<vtkPyramid> pyramid;
  vtkIdList*         ids = pyramid->GetPointIds();

  // Retrieve underlying ids
  vtkIdList* hex = hexa->GetCell(0)->GetPointIds();
  vtkIdType  id0 = hex->GetId(0);
  vtkIdType  id1 = hex->GetId(1);
  vtkIdType  id2 = hex->GetId(2);
  vtkIdType  id3 = hex->GetId(3);
  vtkIdType  id4 = hex->GetId(4);
  vtkIdType  id5 = hex->GetId(5);
  vtkIdType  id6 = hex->GetId(6);
  vtkIdType  id7 = hex->GetId(7);
  vtkIdType  id8 = insert;

  // 6 pyramids linked by a same upper vertex in the cell
  std::array<vtkIdType, 5> p1 = { id0, id1, id2, id3, id8 };
  std::copy(p1.begin(), p1.end(), ids->begin());
  result->InsertNextCell(pyramid->GetCellType(), pyramid->GetPointIds());
  // ---
  std::array<vtkIdType, 5> p2 = { id7, id4, id5, id6, id8 };
  std::copy(p2.begin(), p2.end(), ids->begin());
  result->InsertNextCell(pyramid->GetCellType(), pyramid->GetPointIds());
  // ---
  std::array<vtkIdType, 5> p3 = { id0, id3, id7, id4, id8 };
  std::copy(p3.begin(), p3.end(), ids->begin());
  result->InsertNextCell(pyramid->GetCellType(), pyramid->GetPointIds());
  // // ---
  std::array<vtkIdType, 5> p4 = { id1, id0, id4, id5, id8 };
  std::copy(p4.begin(), p4.end(), ids->begin());
  result->InsertNextCell(pyramid->GetCellType(), pyramid->GetPointIds());
  // // ---
  std::array<vtkIdType, 5> p5 = { id2, id1, id5, id6, id8 };
  std::copy(p5.begin(), p5.end(), ids->begin());
  result->InsertNextCell(pyramid->GetCellType(), pyramid->GetPointIds());
  // // ---
  std::array<vtkIdType, 5> p6 = { id3, id2, id6, id7, id8 };
  std::copy(p6.begin(), p6.end(), ids->begin());
  result->InsertNextCell(pyramid->GetCellType(), pyramid->GetPointIds());

  return true;
}

}

//------------------------------------------------------------------------------
bool vtkMMUnstructuredGridHelperAlgorithm::HexaInsertPoints(
  vtkUnstructuredGrid* hexa, std::vector<vtkIdType> insert, vtkUnstructuredGrid* result)
{
  if (hexa == nullptr)
  {
    std::cerr << "hexa should not be null" << std::endl;
    return false;
  }
  if (hexa->GetNumberOfCells() != 1)
  {
    std::cerr << "hexa should have a single, hexa cell" << std::endl;
    return false;
  }
  if (result == nullptr)
  {
    std::cerr << "result should not be null" << std::endl;
    return false;
  }
  if (result->GetNumberOfCells() || result->GetNumberOfPoints())
  {
    std::cerr << "result is not empty" << std::endl;
  }
  if (insert.empty())
  {
    // Noting to do
    return true;
  }

  // Use the same vtkPoints
  result->SetPoints(hexa->GetPoints());

  if (insert.size() == 1)
  {
    // Need to check additional point position ?
    return CheckHexa(hexa) && HexaInsertOnePoint(hexa, insert[0], result);
  }
  // Only if both grids have a distinct resolution
  if (insert.size() == 2)
  {
    std::cout << "TODO" << std::endl;
  }
  if (insert.size() == 3)
  {
    std::cout << "TODO" << std::endl;
  }
  return false;
}

/// Points related helpers

//------------------------------------------------------------------------------
std::vector<vtkIdType> vtkMMUnstructuredGridHelperAlgorithm::AppendPoints(
  vtkPoints* result, vtkPoints* additional1)
{
  vtkIdType additionalNbPoints = additional1->GetNumberOfPoints();
  vtkIdType initialNbPoints    = result->GetNumberOfPoints();
  result->InsertPoints(initialNbPoints, additionalNbPoints, 0, additional1);
  std::vector<vtkIdType> oldToNew(additionalNbPoints);
  std::iota(oldToNew.begin(), oldToNew.end(), initialNbPoints);
  return oldToNew;
}
