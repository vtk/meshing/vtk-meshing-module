set(vtk_meshing_data_model_files
  vtkMMUniformUG
)

vtk_module_add_module(vtkMMDataModel
  ${FORCE_STATIC_MODULES_STRING}
  CLASSES ${vtk_meshing_data_model_files}
)
