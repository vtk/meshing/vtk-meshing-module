#include "vtkMMUniformUG.h"

// VTK related includes
#include "vtkObjectFactory.h"

vtkStandardNewMacro(vtkMMUniformUG);

//------------------------------------------------------------------------------
void vtkMMUniformUG::PrintSelf(ostream& os, vtkIndent indent)
{
  os << indent << "Internal mesh:";
  if (this->InternalUnstructuredGrid != nullptr)
  {
    this->InternalUnstructuredGrid->PrintSelf(os, indent.GetNextIndent());
  }
  else
  {
    os << "nullptr";
  }
  os << std::endl;
  this->Superclass::PrintSelf(os, indent);
}

//------------------------------------------------------------------------------
bool vtkMMUniformUG::SetInternalUnstructuredGrid(vtkUnstructuredGrid* ug)
{
  if (IsUniform(ug))
  {
    vtkSetObjectBodyMacro(InternalUnstructuredGrid, vtkUnstructuredGrid, ug);
    return true;
  }
  else
  {
    vtkWarningMacro("Input not set, mesh is not uniform");
  }

  return false;
}

//------------------------------------------------------------------------------
bool vtkMMUniformUG::IsUniform(vtkUnstructuredGrid* ug)
{
  vtkNew<vtkCellTypes> cellTypes;
  ug->GetCellTypes(cellTypes);
  return cellTypes->GetNumberOfTypes() == 1;
}
