/**
 * @class   vtkMMUniformUG
 * @brief   Unstrictured grid with a single type of cells
 *
 * This class is still a prototype and is not used yet
 * TODO: a vtkLinkedCellMesh Data Set
 */

#ifndef vtkMMUniformUG_h
#define vtkMMUniformUG_h

// VTK includes
#include "vtkSetGet.h"
#include "vtkUnstructuredGrid.h"

#include "vtkMMDataModelModule.h" // For export macro

// Filter
class VTKMMDATAMODEL_EXPORT vtkMMUniformUG : public vtkUnstructuredGrid
{

public:
  static vtkMMUniformUG* New();
  vtkTypeMacro(vtkMMUniformUG, vtkUnstructuredGrid);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  ///@{
  /**
   * Get / Set the underlying vtkUnstructuredGrid.
   * The conformity is ensured here.
   */
  vtkGetObjectMacro(InternalUnstructuredGrid, vtkUnstructuredGrid);
  virtual bool SetInternalUnstructuredGrid(vtkUnstructuredGrid* ug);
  ///@}

  ///@{
  /**
   * This method is a helper used to unsure a given vtkUnstructuredGrid
   * has a single type of cell.
   */
  ///@}
  static bool IsUniform(vtkUnstructuredGrid* ug);

protected:
  vtkMMUniformUG()           = default;
  ~vtkMMUniformUG() override = default;

  vtkSmartPointer<vtkUnstructuredGrid> InternalUnstructuredGrid;

private:
  vtkMMUniformUG(const vtkMMUniformUG&) = delete;
  void operator=(const vtkMMUniformUG&) = delete;
};

#endif
