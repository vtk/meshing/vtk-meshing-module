/**
 * @class   vtkMMUnstructuredGridMerger
 * @brief   Performs a boolean merge between two (constrained) unstructured grids
 *
 * TODO, explicit constraints on input meshes
 */

#ifndef vtkMMUnstructuredGridMerger_h
#define vtkMMUnstructuredGridMerger_h

#include <set>

#include "vtkMMUnstructuredGridHelperAlgorithm.h"

#include "vtkMMAlgorithmsModule.h" // For export macro

// Helper class,
// contains meshes for an operation, the result
// and other usefull containers like point mapping
struct MeshOperationContainer
{
  vtkUnstructuredGrid* result;
  vtkUnstructuredGrid* m1;
  vtkUnstructuredGrid* m2;
  // map pts2 ids in result
  // no need for pts1: identity
  std::vector<vtkIdType> pts2toResult;
  // list of points included in a cell
  std::vector<std::set<vtkIdType>> pts1inRes;
  std::vector<std::set<vtkIdType>> pts2inRes;

  // Operators

  // Append m1 and m2 points in result
  // fill the pts2toResult map
  bool MergePoints();

  // insert all non overlapping cells between ug1 and ug2 into results
  // Assume points of m1 and m2 are already in result (MergePoints)
  bool InsertNonOvelappingCells();

  // remesh cells from m1 with points from m2 in the result
  bool InsertOverlapM1();

  // Add the missing frontiere
  bool AddFrontiere() {}
};

class VTKMMALGORITHMS_EXPORT vtkMMUnstructuredGridMerger
  : public vtkMMUnstructuredGridHelperAlgorithm
{
public:
  static vtkMMUnstructuredGridMerger* New();
  vtkTypeMacro(vtkMMUnstructuredGridMerger, vtkMMUnstructuredGridHelperAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Set input connection for the second vtkPolyData.
   **/
  void SetSourceConnection(vtkAlgorithmOutput* algOutput);

protected:
  vtkMMUnstructuredGridMerger();
  ~vtkMMUnstructuredGridMerger() override = default;

  // VTK related methods

  int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;

  // Processing

  // Merge points and cells with remesh
  bool MergeUG(MeshOperationContainer& meshOp);

  // Internal fields

  /**
   * if true coincident point are merged in the output data set.
   */
  bool MergePoints = false;

private:
  vtkMMUnstructuredGridMerger(const vtkMMUnstructuredGridMerger&) = delete;
  void operator=(const vtkMMUnstructuredGridMerger&)              = delete;
};

#endif
