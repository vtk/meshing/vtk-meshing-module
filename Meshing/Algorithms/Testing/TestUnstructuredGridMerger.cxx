#include "vtkMMUnstructuredGridMerger.h"

// VTK related
#include <vtkNew.h>
#include <vtkTestUtilities.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>

int TestUnstructuredGridMerger(int, char* argv[])
{
  // Open data

  vtkNew<vtkXMLUnstructuredGridReader> reader1;
  std::string                          cfname(argv[1]);
  cfname += "/image10.vtu";
  // cfname += "/small.vtu";
  reader1->SetFileName(cfname.c_str());

  vtkNew<vtkTransform> translation;
  translation->Translate(5.2, 3.12, 3.1);
  // translation->Translate(-0.45, -0.42, 0.4);

  vtkNew<vtkTransformFilter> move;
  move->SetInputConnection(reader1->GetOutputPort());
  move->SetTransform(translation);

  // Remesh

  vtkNew<vtkMMUnstructuredGridMerger> merger;
  merger->SetInputConnection(0, reader1->GetOutputPort());
  merger->SetInputConnection(1, move->GetOutputPort());

  // Save result

  vtkNew<vtkXMLUnstructuredGridWriter> writer;
  writer->SetInputConnection(merger->GetOutputPort());
  writer->SetFileName("resultUGMerger.vtu");
  writer->SetDataModeToAscii();
  writer->SetCompressorTypeToNone();
  writer->Write();

  return 0;
}
