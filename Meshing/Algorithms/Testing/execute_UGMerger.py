from vtk import (
    vtkClipDataSet,
    vtkPlane,
    vtkRTAnalyticSource,
    vtkTransform,
    vtkTransformFilter
)

from vtk_meshing_module import vtkMMAlgorithms

image = vtkRTAnalyticSource()


# image to unstructured grid through a clip
farAwayPlane = vtkPlane()
farAwayPlane.SetOrigin(1000, 1000, 1000)

clip = vtkClipDataSet()
clip.SetInputConnection(image.GetOutputPort())
clip.SetClipFunction(farAwayPlane)

# generate a second overlapping UG
transform = vtkTransform()

move = vtkTransformFilter()

# instances
merger = vtkMMAlgorithms.vtkMMUnstructuredGridMerger()
merger.SetInputConnection(0, clip.GetOutputPort())
merger.Update()
