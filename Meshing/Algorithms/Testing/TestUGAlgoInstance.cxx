#include <iostream>

#include "vtkMMUnstructuredGridMerger.h"

#include "vtkNew.h"
#include "vtkTestUtilities.h"
#include "vtkXMLUnstructuredGridReader.h"
#include "vtkXMLUnstructuredGridWriter.h"

int TestUGAlgoInstance(int, char* argv[])
{
  // Open data

  // TODO VTU
  vtkNew<vtkXMLUnstructuredGridReader> reader;
  std::string                  cfname(argv[1]);
  cfname += "/dragon.vtp";
  reader->SetFileName(cfname.c_str());

  // Remesh

  vtkNew<vtkMMUnstructuredGridMerger> aw;
  aw->SetInputConnection(reader->GetOutputPort());

  // Save result

  vtkNew<vtkXMLUnstructuredGridWriter> writer;
  writer->SetInputConnection(aw->GetOutputPort());
  writer->SetFileName("alpha_wrapping.vtp");
  writer->Write();

  return 0;
}
