#include "vtkMMUnstructuredGridMerger.h"
#include "vtkMMUniformUG.h"

// STL
#include <cassert>

// VTK related includes
#include "vtkAppendDataSets.h"
#include "vtkCellArrayIterator.h"
#include "vtkCellData.h"
#include "vtkMMUnstructuredGridHelperAlgorithm.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkStaticCellLocator.h"
#include "vtkUnstructuredGrid.h"

vtkStandardNewMacro(vtkMMUnstructuredGridMerger);

//------------------------------------------------------------------------------
// MeshOperationContainer ------------------------------------------------------

//------------------------------------------------------------------------------
bool MeshOperationContainer::MergePoints()
{
  if (this->result->GetPoints() == nullptr)
  {
    vtkNew<vtkPoints> pts;
    this->result->SetPoints(pts);
  }
  vtkPoints* resultPoints = this->result->GetPoints();
  // Add m1
  resultPoints->DeepCopy(this->m1->GetPoints());
  // Add m2 + mapping
  this->pts2toResult =
    vtkMMUnstructuredGridHelperAlgorithm::AppendPoints(resultPoints, this->m2->GetPoints());

  return true;
}

//------------------------------------------------------------------------------
bool MeshOperationContainer::InsertNonOvelappingCells()
{
  // insert m1 cell not overlapping m2

  vtkNew<vtkStaticCellLocator> locator;
  locator->SetDataSet(this->m2);
  locator->BuildLocator();

  pts2inRes.resize(m1->GetNumberOfCells());
  auto it1 = vtk::TakeSmartPointer(this->m1->GetCells()->NewIterator());
  for (it1->GoToFirstCell(); !it1->IsDoneWithTraversal(); it1->GoToNextCell())
  {

    vtkNew<vtkIdList> ptList;
    it1->GetCurrentCell(ptList);
    const vtkIdType cellSize   = ptList->GetNumberOfIds();
    bool            needInsert = true;

    for (vtkIdType i = 0; i < cellSize; i++)
    {
      vtkIdType resId = ptList->GetId(i);

      // Is this point inside an original cell
      double pointPos[3];
      this->result->GetPoint(resId, pointPos);
      vtkIdType enclosingCellId = locator->FindCell(pointPos);
      if (enclosingCellId != -1)
      {
        this->pts2inRes[enclosingCellId].emplace(resId);
        needInsert = false;
      }
    }

    if (needInsert)
    { // no overlap, add the new cell in the this->result mesh
      this->result->InsertNextCell(this->m1->GetCellType(it1->GetCurrentCellId()), ptList);
    }
  }

  // m2 cells not overlapping, need points translation

  locator->SetDataSet(this->m1);
  locator->BuildLocator();

  this->pts1inRes.resize(m2->GetNumberOfCells());
  auto it2 = vtk::TakeSmartPointer(this->m2->GetCells()->NewIterator());
  for (it2->GoToFirstCell(); !it2->IsDoneWithTraversal(); it2->GoToNextCell())
  {

    vtkNew<vtkIdList> ptList;
    it2->GetCurrentCell(ptList);
    const vtkIdType cellSize   = ptList->GetNumberOfIds();
    bool            needInsert = true;

    for (vtkIdType i = 0; i < cellSize; i++)
    {
      vtkIdType addId = ptList->GetId(i);
      vtkIdType resId = this->pts2toResult[addId];
      ptList->SetId(i, resId);

      // Is this point inside an original cell
      double pointPos[3];
      this->result->GetPoint(resId, pointPos);
      vtkIdType enclosingCellId = locator->FindCell(pointPos);
      if (enclosingCellId != -1)
      {
        this->pts1inRes[enclosingCellId].emplace(resId);
        needInsert = false;
      }
    }

    if (needInsert)
    { // no overlap, add the new cell in the this->result mesh
      this->result->InsertNextCell(this->m2->GetCellType(it2->GetCurrentCellId()), ptList);
    }
  }
  return true;
}

//------------------------------------------------------------------------------
bool MeshOperationContainer::InsertOverlapM1()
{
  assert((vtkIdType)pts2inRes.size() == m1->GetNumberOfCells());

  const vtkIdType nbCell1 = pts2inRes.size();
  for (vtkIdType cellId1 = 0; cellId1 < nbCell1; cellId1++)
  {
    // retrive cell Hexa
    vtkCell* hex1 = m1->GetCell(cellId1);
    // to UG
    vtkNew<vtkUnstructuredGrid> remeshCell;
    vtkMMUnstructuredGridHelperAlgorithm::CellToUG(hex1, result->GetPoints(), remeshCell);
    // add points
    // XXX check array name, strange here
    auto&                  listPtsId2 = this->pts1inRes[cellId1];
    std::vector<vtkIdType> insertPoints;
    insertPoints.reserve(listPtsId2.size());
    for (auto id2 : listPtsId2)
    {
      insertPoints.push_back(id2);
    }
    // remesh
    vtkNew<vtkUnstructuredGrid> newCellsUG;
    vtkMMUnstructuredGridHelperAlgorithm::HexaInsertPoints(remeshCell, insertPoints, newCellsUG);
    // insert cells in resutl
    const vtkIdType nbNewCells = newCellsUG->GetNumberOfCells();
    for (vtkIdType i = 0; i < nbNewCells; i++)
    {
      vtkNew<vtkIdList> ptList;
      newCellsUG->GetCellPoints(i, ptList);
      this->result->InsertNextCell(newCellsUG->GetCellType(i), ptList);
    }
  }
  return true;
}

//------------------------------------------------------------------------------
// Main algo -------------------------------------------------------------------

//------------------------------------------------------------------------------
vtkMMUnstructuredGridMerger::vtkMMUnstructuredGridMerger()
{
  this->SetNumberOfInputPorts(2);
}

//------------------------------------------------------------------------------
void vtkMMUnstructuredGridMerger::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//------------------------------------------------------------------------------
void vtkMMUnstructuredGridMerger::SetSourceConnection(vtkAlgorithmOutput* algOutput)
{
  this->SetInputConnection(1, algOutput);
}

//------------------------------------------------------------------------------
int vtkMMUnstructuredGridMerger::RequestData(
  vtkInformation*, vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  // Get the input and source data object
  vtkUnstructuredGrid* inputUG1 = vtkUnstructuredGrid::GetData(inputVector[0]);
  vtkUnstructuredGrid* inputUG2 = vtkUnstructuredGrid::GetData(inputVector[1]);
  vtkUnstructuredGrid* output   = vtkUnstructuredGrid::GetData(outputVector);

  if (!inputUG1 || !inputUG2)
  {
    vtkErrorMacro("Missing input or source.");
    return 0;
  }

  // if meshes do not intersect
  if (not this->DataSetBBIntersect(inputUG1, inputUG2))
  {
    // shortcut, append both and return
    vtkNew<vtkAppendDataSets> append;
    append->SetInputData(inputUG1);
    append->AddInputData(inputUG2);
    append->Update();
    output->ShallowCopy(append->GetOutputDataObject(0));
    return 1;
  }

  // As of now, assume input has only hexa
  if (not vtkMMUniformUG::IsUniform(inputUG1))
  {
    vtkErrorMacro("First input non uniform");
    return 0;
  }
  if (not vtkMMUniformUG::IsUniform(inputUG2))
  {
    vtkErrorMacro("Second input non uniform");
    return 0;
  }

  // Create the MeshOperationContainer

  MeshOperationContainer meshOp;

  vtkNew<vtkUnstructuredGrid> resultMesh;
  resultMesh->AllocateEstimate(1, 10);
  meshOp.result = resultMesh.Get();
  meshOp.m1     = inputUG1;
  meshOp.m2     = inputUG2;

  if (not this->MergeUG(meshOp))
  {
    return 0;
  }

  meshOp.result->GetPointData()->Initialize();
  meshOp.result->GetCellData()->Initialize();

  output->ShallowCopy(meshOp.result);
  return 1;
}

//------------------------------------------------------------------------------
bool vtkMMUnstructuredGridMerger::MergeUG(MeshOperationContainer& meshOp)
{

  // vtkUnstructuredGrid* meshOp.result, vtkUnstructuredGrid* meshOp.m1, vtkUnstructuredGrid*
  // meshOp.m2)
  assert(meshOp.result);

  // Points -----
  meshOp.MergePoints();

  // Cells ------
  meshOp.InsertNonOvelappingCells();
  meshOp.InsertOverlapM1();

  return 1;
}
