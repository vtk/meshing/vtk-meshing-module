vtk_module_find_modules(vtk_meshing_moduule_vtk_modules "${CMAKE_SOURCE_DIR}/Meshing")

paraview_add_plugin(VTKMeshingPlugin
  VERSION "1.0"
  REQUIRED_ON_CLIENT
  REQUIRED_ON_SERVER
  MODULES ${vtk_meshing_provided_modules}
  SERVER_MANAGER_XML
    "VTKMeshingFilters.xml"
)

target_link_libraries(VTKMeshingPlugin
  PRIVATE
    vtkMMDataModel
    vtkMMExecutionModel
    vtkMMAlgorithms)
